---
title: "Rencontres"
date: 2022-04-03T13:01:40+02:00
albumthumb: "rencontres/fagelleur-mental.jpg"
draft: false
## Optional additional meta info for resources list
#  alt: Image alternative and screen-reader text
#  phototitle: A title for the photo
#  description: A sub-title or description for the photo
resources:
- src: "rencontres/fagelleur-mental.jpg"
  weight: -1
  alt: Flagelleur mental
  phototitle: Flagelleur mental
  description: Flagelleur mental recontré dans les égouts.

- src: "rencontres/hart-welkir.jpg"
  weight: 1
  alt: Hart Welkir
  phototitle: Hart Welkir
  description: Malfrat rencontré à la Dague Sanglante.

- src: "rencontres/capitaine-zardoz-zord.jpg"
  weight: 1
  alt: Capitaine Zardoz Zord
  phototitle: Capitaine Zardoz Zord
  description: Capitaine du Carnaval des Sirènes.

- src: "rencontres/victoro-cassalantre.jpg"
  weight: 1
  alt: Victoro Cassalantre
  phototitle: Victoro Cassalantre
  description: Victoro Cassalantre (Noble).

- src: "rencontres/ammalia-cassalantre.jpg"
  weight: 1
  alt: Ammalia Cassalantre
  phototitle: Ammalia Cassalantre
  description: Ammalia Cassalantre (Noble).

- src: "rencontres/willfort-crowelle.jpg"
  weight: 1
  alt: Willfort Crowelle
  phototitle: Willfort Crowelle
  description: Majordome famille Cassalantre.

- src: "rencontres/urstul-floxin.jpg"
  weight: 1
  alt: Urstul Floxin
  phototitle: Urstul Floxin
  description: Boss des malafrats de la villa Gralhund.

- src: "rencontres/orond-yalah-gralhund.jpg"
  weight: 1
  alt: Yalah et Orond Gralhund
  phototitle: Yalah et Orond Gralhund
  description: Yalah et Orond Gralhund (Noble).

- src: "rencontres/vif-acier.png"
  weight: 1
  alt: Vif-acier Gralhund
  phototitle: Vif-acier Gralhund
  description: Le Vif-acier de la famille Gralhund.

- src: "rencontres/rubino-caswell.png"
  weight: 1
  alt: Rubino Caswell
  phototitle: Rubino Caswell (Maître de la Guilde des Constructeurs naval)
  description: Maître de la Guilde des Constructeurs naval.

- src: "rencontres/esvele-rosznar.png"
  weight: 1
  alt: Esvele Rosznar
  phototitle: Esvele Rosznar
  description: Esvele Rosznar (Noble).

- src: "rencontres/kalain-des-neuf-eaux.jpg"
  weight: 1
  alt: Kalain des Neuf Eaux
  phototitle: Kalain des Neuf Eaux
  description: Kalain des Neuf Eaux (peintre, ancienne amante de Dagult Neverember).

---
