---
title: "Auberge du Crâne de Troll"
date: 2022-04-01T19:16:01+02:00
albumthumb: "auberge_crane_de_troll/manoir-crane-de-troll.jpg"
draft: false
## Optional additional meta info for resources list
#  alt: Image alternative and screen-reader text
#  phototitle: A title for the photo
#  description: A sub-title or description for the photo
resources:
- src: "auberge_crane_de_troll/manoir-crane-de-troll.jpg"
  weight: -1
  alt: Le Manoir du Crâne de Troll vue extérieur
  phototitle: Le Manoir du Crâne de Troll vue extérieur
  description: Le Manoir du Crâne de Troll vue extérieur.

- src: "auberge_crane_de_troll/volothamp-geddarm.png"
  weight: 10
  alt: Volothamp Geddarm
  phototitle: Volothamp Geddarm
  description: Le personnage qui a donné le Manoir aux joueurs.

- src: "auberge_crane_de_troll/lif.jpg"
  weight: 11
  alt: Lif
  phototitle: Lif
  description: Personnel de l'auberge - barman elfe fantome.

- src: "auberge_crane_de_troll/lillian-haekin.jpg"
  weight: 12
  alt: Lillian Haekin
  phototitle: Lillian Haekin
  description: Personnel de l'auberge - barmaid originaire de Zakhara.

- src: "auberge_crane_de_troll/yagra-pointdepierre.jpg"
  weight: 13
  alt: Yagra Pointdepierre
  phototitle: Yagra Pointdepierre
  description: Personnel de l'auberge - videur demi-orc.

- src: "auberge_crane_de_troll/elthaea.jpg"
  weight: 14
  alt: Elthaea
  phototitle: Elthaea
  description: Personnel de l'auberge - une des trois serveuses elfes.

- src: "auberge_crane_de_troll/althaea.jpg"
  weight: 15
  alt: Althaea
  phototitle: Althaea
  description: Personnel de l'auberge - une des trois serveuses elfes.
---
