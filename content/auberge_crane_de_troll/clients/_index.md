---
title: "Clients de l'auberge"
date: 2022-04-02T21:56:14+02:00
albumthumb: "auberge_crane_de_troll/clients/ulkoria-moellepierre.jpg"
draft: false
## Optional additional meta info for resources list
#  alt: Image alternative and screen-reader text
#  phototitle: A title for the photo
#  description: A sub-title or description for the photo
resources:
- src: "auberge_crane_de_troll/clients/ulkoria-moellepierre.jpg"
  weight: -1
  alt: Ulkoria Moellepierre
  phototitle: Ulkoria Moellepierre
  description: Naine archimage.

- src: "auberge_crane_de_troll/clients/broxley-bellemarmite.jpg"
  weight: -1
  alt: Broxley Bellemarmite
  phototitle: Broxley Bellemarmite
  description: Représentant du quartier Nord de la Confrérie des tenanciers d’auberge.

- src: "auberge_crane_de_troll/clients/hammond-kraddoc.jpg"
  weight: -1
  alt: Hammond Kraddoc
  phototitle: Hammond Kraddoc
  description: Représentant de la Guilde des vignerons, distillateurs et brasseurs.
---
