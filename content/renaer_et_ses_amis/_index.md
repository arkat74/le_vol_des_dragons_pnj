---
title: "Renaer et ses amis"
date: 2022-04-01T18:49:16+02:00
albumthumb: "renaer_et_ses_amis/renaer-neverember.jpg"
draft: false
## Optional additional meta info for resources list
#  alt: Image alternative and screen-reader text
#  phototitle: A title for the photo
#  description: A sub-title or description for the photo
resources:
- src: "renaer_et_ses_amis/renaer-neverember.jpg"
  weight: -1
  alt: Renaer Neverember
  phototitle: Renaer Neverember
  description: Fils de Dagult Neverember, ancien Seigneur Manifeste de Waterdeep et actuel Seigneur de Neverwinter.

- src: "renaer_et_ses_amis/floon-blagmaar.jpg"
  weight: 1
  alt: Floon Blagmaar
  phototitle: Floon Blagmaar
  description: Ami avec Renaer et Volo.

- src: "renaer_et_ses_amis/eiruk-weskur.png"
  weight: 2
  alt: Eiruk Weskur
  phototitle: Eiruk Weskur
  description: Maitre des Compagnons dans l’Ordre vigilant des Magistes et Protecteurs.

- src: "renaer_et_ses_amis/laraelra-harsard.jpg"
  weight: 3
  alt: Laraelra "Elra" Harsard
  phototitle: Laraelra "Elra" Harsard
  description: Fille de Malaerigo Harsard, le maître de la Guilde des plombiers et excavateurs.

- src: "renaer_et_ses_amis/seigneur-torlyn-wands.jpg"
  weight: 4
  alt: Seigneur Torlyn Wands
  phototitle: Seigneur Torlyn Wands
  description: Chef de la famille noble Wands

- src: "renaer_et_ses_amis/eiruk-weskur.png"
  weight: 5
  alt: Eiruk Weskur
  phototitle: Eiruk Weskur
  description: Maitre des Compagnons dans l’Ordre vigilant des Magistes et Protecteurs.
---
