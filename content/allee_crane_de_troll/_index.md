---
title: "Allée du Crâne-de-troll"
date: 2022-04-06T00:09:16+02:00
albumthumb: "allee_crane_de_troll/talisolvanar-mortebranche.jpg"
draft: false
## Optional additional meta info for resources list
#  alt: Image alternative and screen-reader text
#  phototitle: A title for the photo
#  description: A sub-title or description for the photo
resources:
- src: "allee_crane_de_troll/talisolvanar-mortebranche.jpg"
  weight: -1
  alt: Talisolvanar "Tally" Mortebranche
  phototitle: Talisolvanar "Tally" Mortebranche
  description: Propriétaire et artisan en chef au Clou Tordu, situé à l’extrémité est de l’allée du Crâne-de-troll.

- src: "allee_crane_de_troll/embric.png"
  weight: 1
  alt: Embric
  phototitle: Embric
  description: Marié à Avi. Copropriétaire de Vapeur et Acier, situé à l’extrémité est de l’allée du Crâne-de-troll, appartient à l’Ordre très attentifs des forgerons de talent.

- src: "allee_crane_de_troll/avi.jpg"
  weight: 2
  alt: Avi
  phototitle: Avi
  description: Marié à Embric. Copropriétaire de Vapeur et Acier, situé à l’extrémité est de l’allée du Crâne-de-troll, appartient au Splendide Ordre des armuriers, serruriers et finisseurs.

- src: "allee_crane_de_troll/fala-lefaliir.jpg"
  weight: 3
  alt: Fala Lefaliir
  phototitle: Fala Lefaliir
  description: Possède la Couronne de Corellon, en face du manoir du Crâne-de-troll, de l’autre côté de l’allée du Crâne-de-troll, membre de la Guilde des apothicaires et médecins.

- src: "allee_crane_de_troll/vincent-tranche.jpg"
  weight: 4
  alt: Vincent Tranchée
  phototitle: Vincent Tranchée
  description: Détective privé qui dirige l’Oeil du Tigre, situé à l’extrémité ouest de l’allée du Crâne-de-troll.

- src: "allee_crane_de_troll/rishaal-tourne-pages.jpg"
  weight: 4
  alt: Rishaal Tourne-pages
  phototitle: Rishaal Tourne-pages
  description: Possède la boutique Les Précieux Livres du Dracosire à l’extrémité ouest de l’allée du Crâne-de-troll.
---
